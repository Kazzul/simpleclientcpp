#include "SimpleClient.h"

NotConnectedException::NotConnectedException() : std::exception("TcpClient not connected")
{}


NotConnectedException::NotConnectedException(const char *message) : std::exception(message)
{}


NotConnectedException::~NotConnectedException()
{}


SimpleClient::SimpleClient() : _connected(false)
{
	_stream = new tcp::iostream();
}


SimpleClient::~SimpleClient()
{
	delete _stream;
}


bool SimpleClient::Connect(std::string hostname, std::string port)
{
	_stream->connect(hostname, port);

	_connected = true;

	boost::system::error_code error = _stream->error();
	if (error)
	{
		std::cout << "Error: " << _stream->error().message();
		_connected = false;
	}

	return _connected;
}


void SimpleClient::Run()
{
	if (!_connected)
		throw NotConnectedException();

	std::string userInput;
	boost::system::error_code error;

	ProcessServerResponse();

	std::cout << "Enter the data to be sent: ";

	while (true)
	{
		std::cin >> userInput;

		*_stream << userInput;
		_stream->flush();

		error = _stream->error();
		if (error)
			throw boost::system::system_error(error);

		ProcessServerResponse();

		if (userInput.compare("9") == 0)
			break;

		std::cout << "Enter the data to be sent: ";
	}

	_stream->close();
}

void SimpleClient::ProcessServerResponse()
{
	std::string line;
	std::getline(*_stream, line);
	std::cout << "Server says: " << line << std::endl << std::endl;
}