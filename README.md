# README #

### What is this repository for? ###

* A Simple Networked Client Application. Used to demonstrate basic techniques such as opening a TCP Connection and reading/writing data.
* v1.0

### How do I get set up? ###

* Built in Visual C++ and Visual Studio 2013
* Requires Boost. Expects Boost to be installed to C:\local\boost_1_56_0.
* Can change Boost directories in Project Properties -> VC++ Directories.

### Who do I talk to? ###

* Paul Boocock - Staffordshire University
* paul.boocock@staffs.ac.uk